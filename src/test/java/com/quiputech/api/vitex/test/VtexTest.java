package com.quiputech.api.vitex.test;

import com.quiputech.ecore.v3.commons.utils.Utils;
import com.quiputech.http.util.HttpResponse;
import com.quiputech.http.util.HttpUtil;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import pe.com.visanet.model.AuthorizationResponse;
import pe.com.visanet.api.vtex.controller.VtexController;
import pe.com.visanet.api.vtex.impl.VtexApiServiceImpl;


/**
 *
 * @author Michael Galdámez
 */
public class VtexTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(VtexTest.class);
    private static final String SECURITY_CREDENTIALS = "Basic Z2lhbmNhZ2FsbGFyZG9AZ21haWwuY29tOkF2MyR0cnV6";
    private static final String SECURITY_ENDPOINT = "https://apitestenv.vnforapps.com/api.security/v1/security";
    private static final String AUTH_ENDPOINT = "https://apitestenv.vnforapps.com/api.authorization/v3/authorization/ecommerce/%s";
    private static final String VOID_ENDPOINT = "https://apitestenv.vnforapps.com/api.authorization/v3/reverse/ecommerce/%s";
    private static final String CONFIRM_ENDPOINT = "https://apitestenv.vnforapps.com/api.confirmation/v1/confirmation/ecommerce/%s";
    private static final String CONFIRM_REQUEST = "{\n" +
                                                "  \"terminalId\": \"%s\",\n" +
                                                "  \"channel\": \"%s\",\n" +
                                                "  \"captureType\": \"%s\",\n" +
                                                "  \"order\" : {\n" +
                                                "    \"purchaseNumber\": \"%s\",\n" +
                                                "    \"amount\": %s,\n" +
                                                "    \"currency\": \"%s\",\n" +
                                                "    \"transactionId\": \"%s\"\n" +
                                                "  }\n" +
                                                "}";
    private static final String VOID_REQUEST = "{\n" +
                                                "  \"order\" : {\n" +
                                                "    \"purchaseNumber\": \"%s\",\n" +
                                                "    \"transactionDate\": \"%s\"\n" +
                                                "  }\n" +
                                                "}";
    private static final String AUTH_REQUEST = "{\n" +
                                                "  \"terminalId\": \"%s\",\n" +
                                                "  \"channel\": \"%s\",\n" +
                                                "  \"captureType\": \"%s\",\n" +
                                                "  \"countable\": true,\n" +
                                                "  \"order\" : {\n" +
                                                "    \"purchaseNumber\": \"%s\",\n" +
                                                "    \"amount\": %s,\n" +
                                                "    \"currency\": \"%s\",\n" +
                                                "    \"externalTransactionId\": \"%s\",\n" +
                                                "    \"installment\": 0\n" +
                                                "  },\n" +
                                                "  \"card\": {\n" +
                                                "    \"cardNumber\": \"%s\",\n" +
                                                "    \"expirationMonth\": %s,\n" +
                                                "    \"expirationYear\": %s,\n" +
                                                "    \"cvv2\": \"%s\"\n" +
                                                "  },\n" +
                                                "  \"cardHolder\": {\n" +
                                                "    \"firstName\": \"%s\",\n" +
                                                "    \"lastName\": \"%s\",\n" +
                                                "    \"email\": \"%s\"\n" +
                                                "  }\n" +
                                                "}";
    
    @Test(enabled = false)
    public String getSeguridad() {
        HttpResponse httpResponse = null;
        try {
            httpResponse = HttpUtil.sendRequest(HttpMethod.GET, "", SECURITY_ENDPOINT, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, SECURITY_CREDENTIALS);        
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        
        return httpResponse != null && httpResponse.getHttpCode() == 201 ? httpResponse.getResponse() : null;
    } 
    
    @Test(enabled = false)
    public void getFecha() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        System.out.println("FECHA:" + dateFormat.format(date));
    }    
    
    @Test(enabled = false)
    public void authorize() {
        String accessToken = getSeguridad();
        HttpResponse httpResponse = authorize(accessToken, "341198210", "1", "link", "manual", "1912002", "1.0", "PEN", UUID.randomUUID().toString(), "4041650444437904", 12, 22, "409", "Michael", "Galdamez", "michael.galdamez@quiputech.com");
        AuthorizationResponse response = httpResponse != null && httpResponse.getHttpCode() == 200 ? Utils.fromJson(httpResponse.getResponse(), AuthorizationResponse.class, true) : null;
        if (response != null && response.getOrder() != null && response.getOrder().getActionCode() != null) {
            LOGGER.info(response.getOrder().getActionCode().equalsIgnoreCase("000") ? "Authorized" : "Denied");
        }
    }
    
    @Test(enabled = false)
    public void reverse() {
        String accessToken = getSeguridad();
        String merchantId = "341198210";
        String purchaseNumber = "191209010"; //Debe ser unico
        String trxDate = "20191210"; //Fecha de la transaccion original
        
        HttpResponse httpResponse = authorize(accessToken, merchantId , "1", "link", "manual", purchaseNumber, "1.0", "PEN", UUID.randomUUID().toString(), "4041650444437904", 12, 22, "409", "Michael", "Galdamez", "michael.galdamez@quiputech.com");
        AuthorizationResponse response = httpResponse != null && httpResponse.getHttpCode() == 200 ? Utils.fromJson(httpResponse.getResponse(), AuthorizationResponse.class, true) : null;
        if (response != null && response.getOrder() != null && response.getOrder().getActionCode() != null) {
            LOGGER.info(response.getOrder().getActionCode().equalsIgnoreCase("000") ? "Authorized" : "Denied");
            
            HttpResponse voidResponse = reverse(accessToken, merchantId, purchaseNumber, trxDate);
            AuthorizationResponse resultVoid = voidResponse != null && voidResponse.getHttpCode() == 200 ? Utils.fromJson(voidResponse.getResponse(), AuthorizationResponse.class, true) : null;
            
            if (resultVoid != null) {
                LOGGER.info(resultVoid.getOrder().getActionCode().equalsIgnoreCase("400") ? "Voided" : "Denied");
            }
            
        }
    }
    
    @Test(enabled = false)
    public void confirm() {
        String accessToken = getSeguridad();
        String merchantId = "341198210";
        String purchaseNumber = "191209010"; //Debe ser unico
        
        HttpResponse httpResponse = authorize(accessToken, merchantId , "1", "link", "manual", purchaseNumber, "1.0", "PEN", UUID.randomUUID().toString(), "4041650444437904", 12, 22, "409", "Michael", "Galdamez", "michael.galdamez@quiputech.com");
        AuthorizationResponse response = httpResponse != null && httpResponse.getHttpCode() == 200 ? Utils.fromJson(httpResponse.getResponse(), AuthorizationResponse.class, true) : null;
        if (response != null && response.getOrder() != null && response.getOrder().getActionCode() != null) {
            LOGGER.info(response.getOrder().getActionCode().equalsIgnoreCase("000") ? "Authorized" : "Denied");
            //Confirm solo devuelve header y order
            HttpResponse confirmResponse = confirm(accessToken, merchantId, response.getFulfillment().getTerminalId(), response.getFulfillment().getChannel(), response.getFulfillment().getCaptureType(), response.getOrder().getPurchaseNumber(), String.valueOf(response.getOrder().getAmount()), response.getOrder().getCurrency(), response.getOrder().getTransactionId());
            AuthorizationResponse resultConfirm = confirmResponse != null && confirmResponse.getHttpCode() == 200 ? Utils.fromJson(confirmResponse.getResponse(), AuthorizationResponse.class, true) : null;
            
            if (resultConfirm != null) {
                LOGGER.info(resultConfirm.getOrder().getActionCode().equalsIgnoreCase("000") ? "Confirmed" : "Denied");
            }
            
        }
    }
    
    
    public HttpResponse authorize(String accessToken, String merchantId, String terminalId, 
                            String channel, String captureType, 
                            String purchaseNumber, String amount, 
                            String currency, String externalTransactionId,
                            String cardNumber, int expMonth, int expYear, String cvv2,
                            String firstName, String lastName, String email) {
        HttpResponse httpResponse = null;
        try {
            
            if (accessToken != null) {
                
                
                String body  = String.format(AUTH_REQUEST, terminalId, channel, captureType, purchaseNumber, amount, currency, externalTransactionId, cardNumber, 
                                            String.valueOf(expMonth), String.valueOf(expYear), cvv2, firstName, lastName, email);
                String endpoint = String.format(AUTH_ENDPOINT, merchantId);
                httpResponse = HttpUtil.sendRequest(HttpMethod.POST, body, endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);        
            }
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        
        return httpResponse;
    } 
    
    public HttpResponse reverse(String accessToken, String merchantId, String purchaseNumber, String trxDate) {
        HttpResponse httpResponse = null;
        try {
            
            if (accessToken != null) {               
                String body  = String.format(VOID_REQUEST, purchaseNumber, trxDate);
                String endpoint = String.format(VOID_ENDPOINT, merchantId);
                httpResponse = HttpUtil.sendRequest(HttpMethod.POST, body, endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);        
            }
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        
        return httpResponse;
    } 

     
     public HttpResponse confirm(String accessToken, String merchantId, String terminalId, String channel, String captureType,  String purchaseNumber, String amount, String currency, String trxId) {
        HttpResponse httpResponse = null;
        try {
            if (accessToken != null) {               
                String body  = String.format(CONFIRM_REQUEST, terminalId, channel, captureType, purchaseNumber, amount , currency, trxId);
                String endpoint = String.format(CONFIRM_ENDPOINT, merchantId);
                httpResponse = HttpUtil.sendRequest(HttpMethod.POST, body, endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);        
            }
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        
        return httpResponse;
    } 
     
    @Test(enabled = false)
    public void validateMerchant() {
        String merchantId = "123456789";
        String merchantId1 = "12345A6789";
        String merchantId2 = "12345aws6789";
        String merchantId3 = "Michael";
        if(!StringUtils.isEmpty(merchantId) && StringUtils.isNumeric(merchantId) && StringUtils.length(merchantId) == 9) {
            System.out.println("VALID number");
        }
        if(!StringUtils.isEmpty(merchantId1) && StringUtils.isNumeric(merchantId1) && StringUtils.length(merchantId1) == 9) {
            System.out.println("Invalid number");
        }
        if(!StringUtils.isEmpty(merchantId2) && StringUtils.isNumeric(merchantId2) && StringUtils.length(merchantId2) == 9) {
            System.out.println("Invalid number");
        }
        if(!StringUtils.isEmpty(merchantId3) && StringUtils.isNumeric(merchantId3) && StringUtils.length(merchantId3) == 9) {
            System.out.println("Invalid number");
        }
        if(!StringUtils.isEmpty(merchantId) && StringUtils.isNumeric(merchantId) && StringUtils.length(merchantId) == 9) {
            System.out.println("Invalid number");
        }
    }
    
    @Test(enabled = false)
    public void validateName() {
        VtexApiServiceImpl controller = new VtexApiServiceImpl();
        String firstNametres = "Juan Garcia";
        String firstAndMiddleName = "Juan Carlos";
        String firstNameLastName = "Juan Carlos Garcia";
        String firstNameLastName2 = "Juan";
        String firstNameLastName3 = "Juan Del Carmen Garcia";
        Map<String, String> names = controller.getNames(firstNametres);
        System.out.println("firstName = " + names.get("firstName"));
        System.out.println("lastName = " + names.get("lastName"));
        names = controller.getNames(firstAndMiddleName);
        System.out.println("firstName = " + names.get("firstName"));
        System.out.println("lastName = " + names.get("lastName"));
        names = controller.getNames(firstNameLastName);
        System.out.println("firstName = " + names.get("firstName"));
        System.out.println("lastName = " + names.get("lastName"));
        names = controller.getNames(firstNameLastName2);
        System.out.println("firstName = " + names.get("firstName"));
        System.out.println("lastName = " + names.get("lastName"));
        names = controller.getNames(firstNameLastName3);
        System.out.println("firstName = " + names.get("firstName"));
        System.out.println("lastName = " + names.get("lastName"));
    }
    
    @Test(enabled = false)
    public void getMerchantData() {
        VtexController controller = new VtexController();
        String merchantId = "341198188";
        Map<String, String> merchantData = controller.getMerchantData(merchantId);
        for (Map.Entry<String,String> entry : merchantData.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        }
    }
    
    @Test(enabled = false)
    public void setMaxValue() {
        VtexController controller = new VtexController();
        String amount = "14.89";
        String maxAuthorizedPercentage = "10.00";
        String maxAmount = controller.getMaxValue(amount, maxAuthorizedPercentage);
        System.out.println("maxAmount = " + maxAmount);
    }
}
