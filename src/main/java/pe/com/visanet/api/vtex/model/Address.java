/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Address", description = "Entidad que representa direccion de envio y facturacion")
public class Address {
    private String country;
    private String street;
    private String number;
    private String complement;
    private String neighborhood;
    private String postalCode;
    private String city;
    private String state;

    @ApiModelProperty(value = "Codigo del pais", example = "BRA")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @ApiModelProperty(value = "Nombre de la calle", example = "Praia de Botafogo St.")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @ApiModelProperty(value = "Numero de la calle", example = "300")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @ApiModelProperty(value = "Complemento", example = "3rd Floor")
    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }
    
    @ApiModelProperty(value = "Vecindario", example = "Botafogo")
    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    @ApiModelProperty(value = "Codigo del postal", example = "22250040")
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @ApiModelProperty(value = "Ciudad", example = "Rio de Janeiro")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @ApiModelProperty(value = "Estado", example = "RJ")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
}
