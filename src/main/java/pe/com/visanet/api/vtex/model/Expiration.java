/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Expiration", description = "Entidad que representa los datos de expiracion de una tarjeta de credito")
public class Expiration {
    private String month;
    private String year;

    @ApiModelProperty(value = "Month", example = "12")
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    @ApiModelProperty(value = "Year (two digits)", example = "19")
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
