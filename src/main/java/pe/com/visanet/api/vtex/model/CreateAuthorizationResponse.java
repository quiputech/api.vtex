/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "CreateAuthorizationResponse", description = "Captures (settle) a payment that was previously approved.")
public class CreateAuthorizationResponse {
    private String applicationId;
    private String token;

    @ApiModelProperty(value = "VTEX PaymentId enviado en el request", example = "vtex")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @ApiModelProperty(value = "token", example = "358a5bea-07d0-4122-888a-54ab70b5f02f")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
}
