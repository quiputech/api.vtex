package pe.com.visanet.api.vtex.controller;

import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michael Galdamez
 */
public class VtexBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(VtexBase.class);
    private static final boolean DEBUG = Boolean.parseBoolean(System.getProperty("debug", "false"));
    public static final String HOST = DEBUG
            ? System.getProperty("api.vtex.host", "https://apitestenv.vnforapps.com")
            : System.getProperty("api.vtex.host", "https://apiprod.vnforapps.com");
    public static final boolean CONTROL = Boolean.parseBoolean(System.getProperty("vtex.amountcontrol.enabled", "false"));
    public static final String BIN_LIST = System.getProperty("vtex.amountcontrol.binlist", "491908,491907,463403,463402,463401,457035,457034,457033,455789,450646,450645,448717,434925,431038,431037,428083,428082,428078,428076,428074,428072,421350,409980,407411,402162,402161,402160,400310,377898,377897,377890,377892");
    public static final boolean LOG_ENABLED = Boolean.parseBoolean(System.getProperty("API.VTEX.LOG.ENABLED", "false"));//Save log enabled
    public static final String SECURITY_ENDPOINT = HOST + "/api.security/v1/security";
    public static final String AUTH_ENDPOINT = HOST + "/api.authorization/v3/authorization/ecommerce/{merchantId}";
    public static final String VOID_ENDPOINT = HOST + "/api.authorization/v3/reverse/ecommerce/{merchantId}";
    public static final String CONFIRM_ENDPOINT = HOST + "/api.confirmation/v1/confirmation/ecommerce/{merchantId}";
    public static final String REFUND_ENDPOINT = HOST + "/api.refund/v1/refund/{merchantId}/{transactionId}";
    public static final String TERMINAL = "1";
    public static final String DEFAULT_RUC = "1";
    public static final String CHANNEL = "web"; //pasarela web para producción link para testing
    public static final String CAPTURE_TYPE = "manual";
    public static final String CURRENCY_PEN = "PEN";
    public static final String CURRENCY_USD = "USD";
    public static final String CONFIRMED = "Confirmed";
    public static final String AUTHORIZED = "Authorized";
    public static final String SHOPPER_INTERACTION = "change";
    public static final String MDD14 = "1";
    public static final String PAYMENT_METHOD = "Niubiz";
    public static final String APP_NAME = "niubiz.payment-auth-app";
    public static final String STATUS = "undefined";
    public static final String STATUS_CODE = "200";
    public static final String DELAY_TO_CANCEL = "1200";
    public static final String STATUS_CUSTOM = "CustomError";
    
    public static String formatDecimal(BigDecimal value) {
        LOGGER.trace("Formatting {}", value);
        DecimalFormat numberFormat = new DecimalFormat("###,###,###.00");
        numberFormat.setMinimumIntegerDigits(1);
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setRoundingMode(RoundingMode.DOWN);
        String formatted = numberFormat.format(value.doubleValue());
        LOGGER.trace("Formatted {}", formatted);
        return formatted;
    }
    
    public static JsonObject jsonFromString(String jsonObjectStr) {
        String[] values = jsonObjectStr.split("-");
        values = removeFirstElement(values);
        String body = Arrays.stream(values).collect(Collectors.joining());
        try {
            JsonReader jsonReader = Json.createReader(new StringReader(body.trim()));
            JsonObject object = jsonReader.readObject();
            jsonReader.close();
            return object;
        } catch (JsonException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            JsonObjectBuilder jsonGen = Json.createObjectBuilder();
            jsonGen.add("errorCode", 406);
            jsonGen.add("errorMessage", body.trim());
            return jsonGen.build();
        }
    }
    
    public static String[] removeFirstElement(String[] arr) {
        String[] result = new String[arr.length - 1];
        System.arraycopy(arr,1, result,0, arr.length - 1);
        return result;
    }
    
    public String getMaxValue(String strAmount, String maxAuthorizedPercentage) {
        String maxValue = "";
        try{
            BigDecimal percentage = new BigDecimal(maxAuthorizedPercentage).divide(new BigDecimal(100));
            BigDecimal amount = new BigDecimal(strAmount);
            BigDecimal extraAmount = amount.multiply(percentage);
            BigDecimal maxAmount = amount.add(extraAmount);
            maxValue = formatDecimal(maxAmount);
        } catch(NumberFormatException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return maxValue;
    }
        
    public boolean validateMerchantId(String merchant) {
        return !StringUtils.isEmpty(merchant) && StringUtils.isNumeric(merchant) && StringUtils.length(merchant) <= 9;
    }
    
    public String getStatus(String state) {
        switch(state){
        
            case "Authorized":
            case "Confirmed":
                return "approved";
            case "Not Authorized":
            case "Review":
            case "Reject":
                return "denied";
            default:
                return STATUS;
        }
    }
}
