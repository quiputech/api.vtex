package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Card", description = "Entidad que representa una tarjeta de credito")
public class Card {
    private String holder;
    private String number;
    private String csc;
    private Expiration expiration;

    @ApiModelProperty(value = "Card holder name", example = "John Doe")
    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    @ApiModelProperty(value = "Card number", example = "4041650444437912")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @ApiModelProperty(value = "Card security code", example = "123")    
    public String getCsc() {
        return csc;
    }

    public void setCsc(String csc) {
        this.csc = csc;
    }

    @ApiModelProperty(value = "Expiration entity")
    public Expiration getExpiration() {
        return expiration;
    }

    public void setExpiration(Expiration expiration) {
        this.expiration = expiration;
    }
}
