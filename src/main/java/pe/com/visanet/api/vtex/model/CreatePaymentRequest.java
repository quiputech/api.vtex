package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "createPaymentRequest", description = "Creates a new payment and/or initiates the payment flow.")
public class CreatePaymentRequest {
    private String reference;
    private String orderId;
    private String paymentId;
    private String paymentMethod;
    private String transactionId;
    private String paymentMethodCustomCode;
    private String merchantName;
    private double value;
    private String currency;
    private int installments;
    private String deviceFingerprint;
    private Card card;
    private MiniCart miniCart;
    private String url;
    private String callbackUrl;
    private String returnUrl;
    private String inboundRequestsUrl;
    private String ipAddress;
    private String shopperInteraction;
    private transient boolean sandboxMode;

    @ApiModelProperty(value = "reference", required = true, example = "32478982")
    public String getReference() {
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    @ApiModelProperty(value = "Order Id", required = false, example = "v967373115140abc")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    @ApiModelProperty(value = "paymentId", required = true, example = "191209017")
    public String getPaymentId() {
        return paymentId;
    }

    @JsonProperty("paymentId")
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    @ApiModelProperty(value = "paymentMethod", required = true, example = "Visa")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    @JsonProperty("paymentMethod")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    @ApiModelProperty(value = "Identificador unico de la transaccion", required = false, example = "6fb8b7b7-b98d-4237-873a-afd7c783010d")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    
    @ApiModelProperty(value = "paymentMethod", required = true, example = "")
    public String getPaymentMethodCustomCode() {
        return paymentMethodCustomCode;
    }

    public void setPaymentMethodCustomCode(String paymentMethodCustomCode) {
        this.paymentMethodCustomCode = paymentMethodCustomCode;
    }

    @ApiModelProperty(value = "VTEX merchant name that will receive the payment", required = true, example = "mystore")
    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    @ApiModelProperty(value = "Value amount of the payment", required = true, example = "1.29")
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @ApiModelProperty(value = "Currency code (ISO 4217 alpha-3)", required = true, example = "PEN")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @ApiModelProperty(value = "Number of installments", required = true, example = "3")
    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    @ApiModelProperty(value = "A hash that represents the device used to initiate the payment", required = true, example = "12ade389087fe")
    public String getDeviceFingerprint() {
        return deviceFingerprint;
    }

    public void setDeviceFingerprint(String deviceFingerprint) {
        this.deviceFingerprint = deviceFingerprint;
    }

    @ApiModelProperty(value = "Card", required = true, example = "Card Entity")
    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @ApiModelProperty(value = "MiniCart", required = true, example = "MiniCart Entity")
    public MiniCart getMiniCart() {
        return miniCart;
    }

    public void setMiniCart(MiniCart miniCart) {
        this.miniCart = miniCart;
    }

    @ApiModelProperty(value = "The order URL from merchant's backoffice", required = true, example = "https://admin.mystore.example.com/orders/v32478982")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ApiModelProperty(value = "callbackUrl", required = true, example = "https://api.example.com/some-path/to-notify/status-changes?an=mystore")
    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    @ApiModelProperty(value = "returnUrl", required = true, example = "https://mystore.example.com/checkout/order/v32478982")
    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    @ApiModelProperty(value = "inboundRequestsUrl", required = true, example = "")
    public String getInboundRequestsUrl() {
        return inboundRequestsUrl;
    }

    public void setInboundRequestsUrl(String inboundRequestsUrl) {
        this.inboundRequestsUrl = inboundRequestsUrl;
    }
    
    public String getIpAddress() {
        if (ipAddress == null)
            ipAddress = "";
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @ApiModelProperty(value = "shopperInteraction", required = false, example = "ecommerce")
    public String getShopperInteraction() {
        return shopperInteraction;
    }

    public void setShopperInteraction(String shopperInteraction) {
        this.shopperInteraction = shopperInteraction;
    }

	public boolean isSandboxMode() {
		return sandboxMode;
	}

	public void setSandboxMode(boolean sandboxMode) {
		this.sandboxMode = sandboxMode;
	}
}
