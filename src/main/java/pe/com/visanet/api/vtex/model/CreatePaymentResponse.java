/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "CapturePaymentRequest", description = "Captures (settle) a payment that was previously approved.")
public class CreatePaymentResponse {
    private String paymentId;
    private String status;
    private String tid;
    private String authorizationId;
    private String nsu;
    private String acquirer;
    private String paymentUrl;
    private PaymentAppData paymentAppData;
    private String identificationNumber;
    private String identificationNumberFormatted;
    private String barCodeImageType;
    private String barCodeImageNumber;
    private String code;
    private String message;
    private String delayToAutoSettle;
    private String delayToAutoSettleAfterAntifraud;
    private String delayToCancel;
    private String maxValue;

    @ApiModelProperty(value = "VTEX PaymentId enviado en el request", example = "")
    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    @ApiModelProperty(value = "Provider's payment status:", example = "")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @ApiModelProperty(value = "Provider's unique identifier for the transaction", example = "")
    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }
    @ApiModelProperty(value = "Provider's unique identifier for the authorization", example = "")
    public String getAuthorizationId() {
        return authorizationId;
    }

    public void setAuthorizationId(String authorizationId) {
        this.authorizationId = authorizationId;
    }
    
    @ApiModelProperty(value = "Provider's unique sequential number for the transaction", example = "")
    public String getNsu() {
        return nsu;
    }

    public void setNsu(String nsu) {
        this.nsu = nsu;
    }

    @ApiModelProperty(value = "Acquirer name (mostly used for card payments)", example = "")
    public String getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(String acquirer) {
        this.acquirer = acquirer;
    }
    
    @ApiModelProperty(value = "URL", example = "")
    public String getPaymentUrl() {
        return paymentUrl;
    }

    public void setPaymentUrl(String paymentUrl) {
        this.paymentUrl = paymentUrl;
    }

    //@ApiModelProperty(value = "Indicate the app that will handle the payment flow at Chheckout", example = "")
    public PaymentAppData getPaymentAppData() {
        return paymentAppData;
    }

    public void setPaymentAppData(PaymentAppData paymentAppData) {
        this.paymentAppData = paymentAppData;
    }
    @ApiModelProperty(value = "The bank invoice unformatted identification number", example = "")
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }
    
    @ApiModelProperty(value = "The bank invoice formatted identification number that will be presented to the end user", example = "")
    public String getIdentificationNumberFormatted() {
        return identificationNumberFormatted;
    }

    public void setIdentificationNumberFormatted(String identificationNumberFormatted) {
        this.identificationNumberFormatted = identificationNumberFormatted;
    }
    @ApiModelProperty(value = "barCodeImageType", example = "")
    public String getBarCodeImageType() {
        return barCodeImageType;
    }

    public void setBarCodeImageType(String barCodeImageType) {
        this.barCodeImageType = barCodeImageType;
    }
    
    @ApiModelProperty(value = "barCodeImageNumber", example = "")   
    public String getBarCodeImageNumber() {
        return barCodeImageNumber;
    }

    public void setBarCodeImageNumber(String barCodeImageNumber) {
        this.barCodeImageNumber = barCodeImageNumber;
    }
    
    @ApiModelProperty(value = "Code", example = "")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    @ApiModelProperty(value = "Message", example = "")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    @ApiModelProperty(value = "delayToAutoSettle", example = "")
    public String getDelayToAutoSettle() {
        return delayToAutoSettle;
    }

    public void setDelayToAutoSettle(String delayToAutoSettle) {
        this.delayToAutoSettle = delayToAutoSettle;
    }
    
    @ApiModelProperty(value = "delayToAutoSettleAfterAntifraud", example = "")
    public String getDelayToAutoSettleAfterAntifraud() {
        return delayToAutoSettleAfterAntifraud;
    }

    public void setDelayToAutoSettleAfterAntifraud(String delayToAutoSettleAfterAntifraud) {
        this.delayToAutoSettleAfterAntifraud = delayToAutoSettleAfterAntifraud;
    }
    
    @ApiModelProperty(value = "delayToCancel", example = "")
    public String getDelayToCancel() {
        return delayToCancel;
    }

    public void setDelayToCancel(String delayToCancel) {
        this.delayToCancel = delayToCancel;
    }
    
    @ApiModelProperty(value = "maxValue", example = "16.67")
    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }
    
	@Override
	public String toString() {
		return "CreatePaymentResponse [paymentId=" + paymentId + ", status=" + status + ", tid=" + tid
				+ ", authorizationId=" + authorizationId + ", nsu=" + nsu + ", acquirer=" + acquirer + ", paymentUrl="
				+ paymentUrl + ", paymentAppData=" + paymentAppData + ", identificationNumber=" + identificationNumber
				+ ", identificationNumberFormatted=" + identificationNumberFormatted + ", barCodeImageType="
				+ barCodeImageType + ", barCodeImageNumber=" + barCodeImageNumber + ", code=" + code + ", message="
				+ message + ", delayToAutoSettle=" + delayToAutoSettle + ", delayToAutoSettleAfterAntifraud="
				+ delayToAutoSettleAfterAntifraud + ", delayToCancel=" + delayToCancel + ", maxValue=" + maxValue + "]";
	}
    
    

}
