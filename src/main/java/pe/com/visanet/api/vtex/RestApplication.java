package pe.com.visanet.api.vtex;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * @author Michael Galdamez
 */

@ApplicationPath("/v1")
public class RestApplication extends Application {

}
