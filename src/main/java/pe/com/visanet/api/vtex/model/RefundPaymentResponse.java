/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "RefundPaymentResponse", description = "Refunds a payment that was previously captured (settled).")
public class RefundPaymentResponse {
    private String paymentId;
    private String refundId;
    private double value;
    private String code;
    private String message;
    private String requestId;

    @ApiModelProperty(value = "VTEX PaymentId enviado en el request", example = "191209022")
    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }
    
    @ApiModelProperty(value = "Provider's refund identifier", example = "f10d406c-7c73-4696-b0d9-8a1ca0b83972")
    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    @ApiModelProperty(value = "The amount that was refunded ", example = "12.99")
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @ApiModelProperty(value = "Provider's operation/error code to be logged", example = "200")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ApiModelProperty(value = "Provider's operation/error message to be logged", example = "Refunded")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @ApiModelProperty(value = "RequestId enviado en el request", example = "D12D9B80972C462980F5067A3A126845")
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
