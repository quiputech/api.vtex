/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex;

import com.quiputech.http.util.HttpResponse;
import pe.com.visanet.api.vtex.model.CreatePaymentRequest;

/**
 *
 * @author mgaldamez
 */
public interface VtexApiService {
    public HttpResponse authorize(CreatePaymentRequest body, String merchant, String accessToken);
    public HttpResponse confirm(String appToken, String merchant, String purchaseNumber, String authorizedAmount, String amount, String currency, String trxId);
    public HttpResponse reverse(String appToken, String merchant, String purchaseNumber, String trxDate);
    public HttpResponse refund(String appToken, String merchant, String transactionId, String ruc, String amount);
    public String securityToken(String securityCredentials);
}
