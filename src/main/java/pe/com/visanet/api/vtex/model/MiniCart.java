/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Expiration", description = "Entidad que representa el minicart")
public class MiniCart {
    private double shippingValue;
    private double taxValue;
    private Buyer buyer;
    private Address shippingAddress;
    private Address billingAddress;
    private Items[] items;

    @ApiModelProperty(value = "Shipping Value", example = "12")
    public double getShippingValue() {
        return shippingValue;
    }

    public void setShippingValue(double shippingValue) {
        this.shippingValue = shippingValue;
    }

    
    @ApiModelProperty(value = "Total Tax Value", example = "1234")
    public double getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(double taxValue) {
        this.taxValue = taxValue;
    }

    
    @ApiModelProperty(value = "Buyer", example = "Buyer Entity")
    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    
    @ApiModelProperty(value = "Shipping Address", example = "Address Entity")
    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    
    @ApiModelProperty(value = "Billing Address", example = "Address Entity")
    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    
    @ApiModelProperty(value = "Arreglo de items", example = "Items Entity")
    public Items[] getItems() {
        return items;
    }

    public void setItems(Items[] items) {
        this.items = items;
    }
}
