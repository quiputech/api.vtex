/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@ApiModel(value = "refundResponse", description = "Respuesta para refund")
public class RefundResponse {

    private int errorCode;
    private String errorMessage;
    private long millis;
    private String transactionUUID;
    private long transactionDate;
    private HashMap<String, String> data = new HashMap<>();
    private final LinkedHashMap<Integer, String> transactionLog = new LinkedHashMap<>();

    /**
     * @return the transactionUUID
     */
    @ApiModelProperty(value = "Transaction UUID", required = true, example = "3e8a4e94-46c6-4e5e-8c1e-eea7ede466d4")
    public String getTransactionUUID() {
        return transactionUUID;
    }


    /**
     * @return the transactionDate
     */
    @ApiModelProperty(value = "Fecha de la transacción", required = true, example = "1561421520148")
    public long getTransactionDate() {
        return transactionDate;
    }


    /**
     * @return the errorCode
     */
    @ApiModelProperty(value = "Código de error", required = true, example = "110")
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorMessage
     */
    @ApiModelProperty(value = "Mensaje de error", required = true, example = "No existen devoluciones para la consulta realizada.")
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the millis
     */
    @ApiModelProperty(value = "Milis", required = true, example = "1834")
    public long getMillis() {
        return millis;
    }

    /**
     * @param millis the millis to set
     */
    public void setMillis(long millis) {
        this.millis = millis;
    }

    /**
     * @return the data
     */
    @ApiModelProperty(value = "data", required = false, example = "508565935")
    public HashMap<String, String> getData() {
        return data;
    }

    /**
     * @return the transactionLog
     */
    @ApiModelProperty(value = "TransactionLog", required = false, example = "")
    public LinkedHashMap<Integer, String> getTransactionLog() {
        return transactionLog;
    }

    /**
     * @param data the data to set
     */
    public void setData(HashMap<String, String> data) {
        this.data = data;
    }
    
    public void setTransactionUUID(String transactionUUID) {
        this.transactionUUID = transactionUUID;
    }

    public void setTransactionDate(long transactionDate) {
        this.transactionDate = transactionDate;
    }

}