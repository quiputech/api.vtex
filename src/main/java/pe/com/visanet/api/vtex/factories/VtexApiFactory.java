package pe.com.visanet.api.vtex.factories;

import pe.com.visanet.api.vtex.VtexApiService;
import pe.com.visanet.api.vtex.impl.VtexApiServiceImpl;

/**
 *
 * @author mgaldamez
 */
public class VtexApiFactory {
    private final static VtexApiService SERVICE = new VtexApiServiceImpl();

    public static VtexApiService getVtexApi()
    {
       return SERVICE;
    }
}
