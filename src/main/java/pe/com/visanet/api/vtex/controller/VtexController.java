package pe.com.visanet.api.vtex.controller;

import com.quiputech.ecore.v3.commons.utils.Utils;
import com.quiputech.http.util.HttpResponse;
import java.util.Map;

import javax.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.com.visanet.api.vtex.driver.VtexDriver;
import pe.com.visanet.api.vtex.VtexApiService;
import pe.com.visanet.api.vtex.factories.VtexApiFactory;
import pe.com.visanet.api.vtex.model.*;
import pe.com.visanet.model.AuthorizationResponse;
import com.quiputech.http.util.HttpUtil;
import java.io.IOException;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Michael Galdámez
 */
public class VtexController extends VtexBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(VtexController.class);
    private final VtexApiService delegate = VtexApiFactory.getVtexApi();
    
    public Map<String, String> getMerchantData(String merchant) {
        VtexDriver driver = new VtexDriver();
        return driver.getMerchantData(merchant);
    }

    public void insertTransaction(String paymentId, String merchantId, String operation, String transactionId, String requestId, String purchaseNumber,
        String request, String response, String amount, String status) {
        VtexDriver driver = new VtexDriver();
        driver.insertTransaction(paymentId, merchantId, operation, transactionId, requestId, purchaseNumber, request, response, amount, status);
    }
    
    public Map<String, String> getPayment(String paymentId, String merchantId, String state) {
        VtexDriver driver = new VtexDriver();
        return driver.getPayment(paymentId, merchantId, state);
    }
    
    public CreatePaymentResponse createPayment(String merchant, String appToken, CreatePaymentRequest body, String isTestSuite) {
        HttpResponse httpResponse = null;
        VtexDriver driver = new VtexDriver();
        CreatePaymentResponse paymentResponse = null;
        
        body.setInstallments(body.getInstallments() == 1 ? 0 : body.getInstallments());
        LOGGER.info("Create_Payment");
        LOGGER.info("Request vtex: {}", Utils.toJson(body));
        String amount = String.valueOf(body.getValue());

        Map<String, String> merchantData = getMerchantData(merchant);

        if (!body.getPaymentMethod().equals(PAYMENT_METHOD)) {
        	//CA:05 TD-813
            httpResponse = delegate.authorize(body, merchant, appToken);
        } else {
        	//paymentMethod = Niubiz
            String accessToken = delegate.securityToken(appToken);
            paymentResponse = new CreatePaymentResponse();
            PaymentAppData paymentAppData = new PaymentAppData();
            PaymentPayloadAppData payload = new PaymentPayloadAppData();

            Map<String,String> map = driver.getPaymentUndefined(body.getReference(), merchant);
            if (map != null && map.size() > 0) {
            	//status approved or denied
                LOGGER.info("Transaction found");
                paymentResponse.setTid(map.get("signature"));
                String status = getStatus(map.get("state"));
                paymentResponse.setStatus(status);
                paymentResponse.setAuthorizationId(map.get("id_procesador"));
                paymentResponse.setNsu(map.get("authorization_code"));
                paymentResponse.setAcquirer(map.get("entity_id"));
                paymentResponse.setCode(map.get("action_code"));
                if (map.get("action_code").equals("000") || map.get("action_code").equals("010")) {
                    paymentResponse.setMessage("Aprobado y completado con exito");
                } else {
                    paymentResponse.setMessage(map.get("authorization_message"));
                }
                
            	//CA:02 & CA:03 TD-813
            		//executeCallback(merchant,body, paymentResponse, amount, false);

            } else {
            	//status undefined
                paymentResponse.setStatus(STATUS);
                paymentResponse.setCode(STATUS_CODE);
                paymentResponse.setDelayToCancel(DELAY_TO_CANCEL);
                //CA:07 & 08 TD-813
                //body.setSandboxMode(body.isSandboxMode() ? true : false);
                payload.setMerchant(merchant);
                payload.setAccessToken(accessToken);
                payload.setReference(body.getReference());
                payload.setOrderId(body.getOrderId());
                payload.setTransactionId(body.getTransactionId());
                payload.setPaymentId(body.getPaymentId());
                payload.setMiniCart(body.getMiniCart());
                payload.setSandboxMode(body.isSandboxMode() ? true : false);
                paymentAppData.setAppName(APP_NAME);
                paymentAppData.setPayload(Utils.toJson(payload).replaceAll("(\n|\r)", ""));
                paymentResponse.setPaymentAppData(paymentAppData);
                paymentResponse.setMessage("");
                                
                //CA:04 TD-813
                if (isTestSuite != null && Boolean.valueOf(isTestSuite)) {
                    //VTEX-API-Is-TestSuite = TRUE
                	//callback execution
                    executeCallback(merchant,body, paymentResponse, amount, true);
                    //Set some values for response
                	//CA:17 TD-813
                    paymentResponse.setPaymentUrl(body.getCallbackUrl());
                    //CA:18 TD-813
                    paymentResponse.setAuthorizationId("PENDING200");
                    //CA:01 & CA:09 TD-813
                    //String tId= body.getReference()!=null ? new StringBuilder().append("TID-").append(body.getReference()).toString() : null;
                    //paymentResponse.setTid(tId);
                    //CA:14 TD-813
                    //String nsu= body.getReference()!=null ? new StringBuilder().append("NSU-").append(body.getReference()).toString() : null;
                    //paymentResponse.setNsu(nsu);
                    
                }else {
                	//Set some values for response
                	//VTEX-API-Is-TestSuite = FALSE o NO EXISTE
                    //CA:10 TD-813
                    paymentResponse.setPaymentUrl(null);
                    //CA:15 TD-813
                    paymentResponse.setAuthorizationId(null);
                    //Set some values for response
                    paymentResponse.setTid(null);
                    //Set some values for response
                    paymentResponse.setNsu(null);
                }
                               
                //set the undefined status to response
                paymentResponse.setStatus(STATUS);
                //CA:16 TD-813
                paymentResponse.setPaymentAppData(paymentAppData);
            }
            paymentResponse.setPaymentId(body.getPaymentId());
        }
        //LOGGER.info("paymentResponse: {}", Utils.toJson(paymentResponse.toString()));
        //Check the value of httpResponse supporting null values
        LOGGER.info("httpResponse: {}", httpResponse == null ? String.valueOf(httpResponse) : httpResponse.getResponse());

        if (httpResponse != null ) {
            if (httpResponse.getHttpCode() == 200) {
                AuthorizationResponse response = Utils.fromJson(httpResponse.getResponse(), AuthorizationResponse.class, true);
                if (response != null) {
                    if((response.getDataMap() != null || (response.getData() != null && !response.getData().isEmpty()))) {
                        paymentResponse = new CreatePaymentResponse();
                        paymentResponse.setPaymentId(body.getPaymentId());
                        if(response.getHeader() != null) {
                            paymentResponse.setTid(response.getHeader().getEcoreTransactionUUID());
                        }
                        if(response.getDataMap() != null) {
                            paymentResponse.setStatus(
                                 response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("000") || 
                                 response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("010") ? "approved" : "denied"
                            );

                            paymentResponse.setAuthorizationId(response.getDataMap().get("ID_UNICO"));
                            paymentResponse.setNsu(response.getDataMap().get("AUTHORIZATION_CODE"));                   
                            paymentResponse.setAcquirer(response.getDataMap().get("ADQUIRENTE"));
                            paymentResponse.setCode(response.getDataMap().get("ACTION_CODE"));
                            paymentResponse.setMessage(response.getDataMap().get("ACTION_DESCRIPTION"));
                        } else {
                            paymentResponse.setStatus(
                                response.getData().get("ACTION_CODE").equalsIgnoreCase("000") || 
                                response.getData().get("ACTION_CODE").equalsIgnoreCase("010") ? "approved" : "denied"
                            );

                            paymentResponse.setAuthorizationId("");
                            paymentResponse.setNsu("");                   
                            paymentResponse.setAcquirer("");
                            paymentResponse.setCode(response.getData().get("ACTION_CODE"));
                            paymentResponse.setMessage(response.getData().get("ACTION_DESCRIPTION"));
                        }
                        paymentResponse.setDelayToAutoSettle(null);
                        paymentResponse.setDelayToAutoSettleAfterAntifraud(null);
                        paymentResponse.setDelayToCancel(null);
                    }
                }
            } else {
                paymentResponse = new CreatePaymentResponse();
                paymentResponse.setStatus(STATUS_CUSTOM);
                JsonObject jsonResp = jsonFromString(httpResponse.toString());
                String errorCode = String.valueOf(jsonResp.getInt("errorCode", 400));
                String errorMsg = jsonResp.getString("errorMessage", "");
                paymentResponse.setCode(errorCode);
                paymentResponse.setMessage(errorMsg);
                return paymentResponse;
            }
        }
        
        if (paymentResponse != null && merchantData != null) {
            String maxAmount = merchantData.get("maxAmount");
            int automatedSettle = Integer.valueOf(merchantData.get("automatedSettle"));
            if (automatedSettle > 0) {
                paymentResponse.setMaxValue(amount);
            } else {
                paymentResponse.setMaxValue(getMaxValue(amount, maxAmount));
            } 
        }
        
        if (paymentResponse != null) {
            String response = Utils.toJson(paymentResponse);
            String status = paymentResponse.getCode();
            //CA:11 & 12 TD-813
            String operation = paymentResponse.getStatus().equalsIgnoreCase(STATUS) ? "ASYNC" : "AUTH";
            insertTransaction(body.getPaymentId(), merchant, operation, body.getTransactionId(), 
            "NULL", body.getReference(), Utils.toJson(body), response, amount, status);
        }
        
        return paymentResponse;
    }

    
    public CancelPaymentResponse cancelPayment(String paymentId, String merchant, String appToken, CancelPaymentRequest body) {
        CancelPaymentResponse paymentResponse = null;

        LOGGER.info("Cancel_Payment");
        LOGGER.info("Request vtex: {}",Utils.toJson(body));
        String requestId = body.getRequestId();
        if (StringUtils.isEmpty(merchant) || !StringUtils.isNumeric(merchant) || StringUtils.length(merchant) > 9) {
            LOGGER.error("Invalid Merchant");
            return paymentResponse;
        }

        Map<String,String> map = getPayment(paymentId, merchant, AUTHORIZED);
        if(map == null || map.size() < 1) {
            LOGGER.error("Transaction not found");
            return paymentResponse;
        }
        LOGGER.info("Transaction found");
        String trxDate = map.get("transactionDate");
        String purchaseNumber = map.get("purchaseNumber");
        LOGGER.info("Transaction Date: {}", map.get("transactionDate"));

        HttpResponse httpResponse = delegate.reverse(appToken, merchant, purchaseNumber, trxDate);

        if(httpResponse != null && httpResponse.getHttpCode() == 403) {
            paymentResponse = new CancelPaymentResponse();
            paymentResponse.setPaymentId(paymentId);
            paymentResponse.setCancellationId(null);
            paymentResponse.setCode("403");
            paymentResponse.setMessage(httpResponse.getResponse());
            paymentResponse.setRequestId(requestId);
            insertTransaction(body.getPaymentId(), merchant, "REVERSE", "", 
            body.getRequestId(), purchaseNumber, Utils.toJson(body), Utils.toJson(paymentResponse), map.get("amount"), paymentResponse.getCode());
            return paymentResponse;
        }
        AuthorizationResponse response = httpResponse != null && httpResponse.getHttpCode() == 200 ? Utils.fromJson(httpResponse.getResponse(), AuthorizationResponse.class, true) : null;
        if (response == null) {
            LOGGER.error("Response NULL");
            return paymentResponse; 
        }
        if((response.getDataMap() != null || response.getData() != null)) {
            if(response.getDataMap() != null) {
                paymentResponse = new CancelPaymentResponse();
                paymentResponse.setPaymentId(paymentId);
                paymentResponse.setCode(response.getDataMap().get("ACTION_CODE"));
                paymentResponse.setMessage(response.getDataMap().get("STATUS"));
                paymentResponse.setRequestId(requestId);
                if(response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("400")) {
                    LOGGER.info(response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("400") ? "Voided" : "Denied");                    
                    paymentResponse.setCancellationId(response.getHeader().getEcoreTransactionUUID());
                    insertTransaction(body.getPaymentId(), merchant, "REVERSE", "", 
                    body.getRequestId(), purchaseNumber, Utils.toJson(body), Utils.toJson(paymentResponse), map.get("amount"), paymentResponse.getCode());
                    return paymentResponse;  
                } else {                       
                    paymentResponse.setCancellationId(null);
                    insertTransaction(body.getPaymentId(), merchant, "REVERSE", "", 
                    body.getRequestId(), purchaseNumber, Utils.toJson(body), Utils.toJson(paymentResponse), map.get("amount"), paymentResponse.getCode());
                    return paymentResponse;
                }
            } 
            LOGGER.error("Response Datamap null");
            LOGGER.error("Action_Code: {}", response.getData().get("ACTION_CODE"));
            LOGGER.error("Status: {}", response.getData().get("STATUS"));                      
        }
        return paymentResponse;
    }

    public CapturePaymentResponse capturePayment(String paymentId, String merchant, String appToken, CapturePaymentRequest body) {
        CapturePaymentResponse paymentResponse = null;
    
        LOGGER.info("Capture_Payment");
        LOGGER.info("Request vtex: {}", Utils.toJson(body));

        String requestId = body.getRequestId();
        if (StringUtils.isEmpty(paymentId)) {
            LOGGER.info("Payment Id is missing");
            return paymentResponse;
        }

        if (StringUtils.isEmpty(merchant) || !StringUtils.isNumeric(merchant) || StringUtils.length(merchant) > 9) {
            LOGGER.error("Invalid Merchant");
            return paymentResponse;
        }

        Map<String,String> map = getPayment(paymentId, merchant, AUTHORIZED);

        if (map == null || map.size() < 1)  {
            LOGGER.info("Transaction not found");
            return paymentResponse;
        }
        LOGGER.info("Transaction found");

        String purchaseNumber = map.get("purchaseNumber");
        String authorizationId = map.get("id_procesador"); //authorizationId del response
        String currency = map.get("currency");
        String amount = map.get("amount");

        HttpResponse httpResponse = delegate.confirm(appToken, merchant, purchaseNumber, String.valueOf(body.getValue()), amount, currency, authorizationId);

        AuthorizationResponse response = httpResponse != null && httpResponse.getHttpCode() == 200 ? Utils.fromJson(httpResponse.getResponse(), AuthorizationResponse.class, true) : null;
        if (httpResponse != null && httpResponse.getHttpCode() == 403) {
            paymentResponse = new CapturePaymentResponse();
            paymentResponse.setPaymentId(paymentId);
            paymentResponse.setSettleId(null);
            paymentResponse.setValue(0);
            paymentResponse.setCode("403");
            paymentResponse.setMessage(httpResponse.getResponse());
            paymentResponse.setRequestId(requestId);
            insertTransaction(body.getPaymentId(), merchant, "CONFIRM", body.getTransactionId(), 
            "NULL", purchaseNumber, Utils.toJson(body), Utils.toJson(paymentResponse), String.valueOf(body.getValue()), paymentResponse.getCode());
            return paymentResponse;  
        }
        if (response == null) { 
            LOGGER.error("Response NULL");
            return paymentResponse;  
        }
        if ((response.getDataMap() != null || response.getData() != null)) {
            if (response.getDataMap() != null) {
                paymentResponse = new CapturePaymentResponse();
                paymentResponse.setPaymentId(paymentId);
                if (response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("000") || response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("010")) {
                    LOGGER.info(response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("000") || 
                                response.getDataMap().get("ACTION_CODE").equalsIgnoreCase("010") ? "Confirmed" : "Denied");                                        
                    paymentResponse.setSettleId(response.getHeader().getEcoreTransactionUUID());
                    paymentResponse.setValue(response.getOrder().getAmount());
                    paymentResponse.setCode(response.getDataMap().get("ACTION_CODE"));
                    paymentResponse.setMessage(response.getDataMap().get("ACTION_DESCRIPTION"));
                    paymentResponse.setRequestId(requestId);
                    insertTransaction(body.getPaymentId(), merchant, "CONFIRM", body.getTransactionId(), 
                    "NULL", purchaseNumber, Utils.toJson(body), Utils.toJson(paymentResponse), String.valueOf(body.getValue()), paymentResponse.getCode());
                    return paymentResponse;
                } else { 
                    paymentResponse.setSettleId(null);
                    paymentResponse.setValue(0);
                    paymentResponse.setCode(response.getDataMap().get("ACTION_CODE"));
                    paymentResponse.setMessage(response.getDataMap().get("ACTION_DESCRIPTION"));
                    paymentResponse.setRequestId(requestId);
                    insertTransaction(body.getPaymentId(), merchant, "CONFIRM", body.getTransactionId(), 
                    "NULL", purchaseNumber, Utils.toJson(body), Utils.toJson(paymentResponse), String.valueOf(body.getValue()), paymentResponse.getCode());
                    return paymentResponse;  
                }
            }
            LOGGER.error("Response Datamap null");
            LOGGER.error("Action_Code: {}", response.getData().get("ACTION_CODE"));
            LOGGER.error("Action_Description: {}", response.getData().get("ACTION_DESCRIPTION"));   
        }
        return paymentResponse;
    }
    
    public RefundPaymentResponse refundPayment(String paymentId, String merchant, String appToken, RefundPaymentRequest body) { 
        RefundPaymentResponse paymentResponse = null;

        LOGGER.info("Refund_Payment");
        LOGGER.info("Request vtex: {}", Utils.toJson(body));
        String requestId = body.getRequestId();

        if (StringUtils.isEmpty(merchant) || !StringUtils.isNumeric(merchant) || StringUtils.length(merchant) > 9) {
            LOGGER.error("Invalid Merchant");
            return paymentResponse;
        }

        Map<String,String> map = getPayment(paymentId, merchant, CONFIRMED);

        if (map == null || map.size() < 1) {
            LOGGER.info("Transaction not found");
            return paymentResponse;
        }
        LOGGER.info("Transaction found");
        String transactionId = map.get("id_procesador");
        Map<String, String> merchantData = getMerchantData(merchant);
        String ruc = merchantData.get("ruc");
        if(ruc == null) {
            LOGGER.info("RUC is null");
            return paymentResponse;
        }
        HttpResponse httpResponse = delegate.refund(appToken, merchant, transactionId, ruc, String.valueOf(body.getValue()));

        RefundResponse response = httpResponse != null && httpResponse.getHttpCode() == 200 ? Utils.fromJson(httpResponse.getResponse(), RefundResponse.class, true) : null;
        if (httpResponse != null && httpResponse.getHttpCode() == 403) {
            paymentResponse = new RefundPaymentResponse();
            paymentResponse.setPaymentId(paymentId);
            paymentResponse.setRefundId(null);
            paymentResponse.setValue(0);
            paymentResponse.setCode("403");
            paymentResponse.setMessage(httpResponse.getResponse());
            paymentResponse.setRequestId(requestId);
            insertTransaction(body.getPaymentId(), merchant, "REFUND", body.getTransactionId(), 
            "NULL", "", Utils.toJson(body), Utils.toJson(paymentResponse), body.getValue(), paymentResponse.getCode());
            return paymentResponse;
        }
        if (response != null && response.getData() != null) {
            paymentResponse = new RefundPaymentResponse();
            paymentResponse.setPaymentId(paymentId);
            paymentResponse.setRequestId(requestId);
            if (response.getData().get("CODERROR").equals("100")) {
                LOGGER.info(response.getData().get("CODERROR").equals("100") ? "Refunded" : "Denied"); 
                paymentResponse.setRefundId(response.getTransactionUUID());
                paymentResponse.setValue(Double.valueOf(body.getValue()));
                paymentResponse.setCode(response.getData().get("CODERROR"));
                paymentResponse.setMessage("Successfully refunded"); //response.getData().get("DSCERROR")
                insertTransaction(body.getPaymentId(), merchant, "REFUND", body.getTransactionId(), 
                "NULL", "", Utils.toJson(body), Utils.toJson(paymentResponse), body.getValue(), paymentResponse.getCode());
                return paymentResponse;
            } else {
                if (response.getData().get("CODERROR").equals("110")) {
                    paymentResponse.setRefundId(null);
                    paymentResponse.setValue(0);
                    paymentResponse.setCode("refund-manually");
                    paymentResponse.setMessage("This payment needs to be manually refunded");
                    insertTransaction(body.getPaymentId(), merchant, "REFUND", body.getTransactionId(), 
                    "NULL", "", Utils.toJson(body), Utils.toJson(paymentResponse), body.getValue(), paymentResponse.getCode());
                    return paymentResponse;
                }
            }
        }
        return paymentResponse;
    }
    
	/**
	 * @param merchant
	 * @param body
	 * @param paymentResponse
	 * @param amount
	 */
	private void executeCallback(String merchant, CreatePaymentRequest body,CreatePaymentResponse paymentResponse, 
								 String amount, boolean isUndefined) {
		try {
			
			HttpResponse httpResponse = null;
		    String AppKey = "vtexappkey-visanetpe-QFBUXL";
		    String AppToken = "VOEFFUJQDBFNVUZNWOSCIPVEWYAIGQULRYGFPKTTNRFOESJHQILBWIRJHXLZKARTZHMZTAAJUXIXPFSVDCQAACEGIDTYOODJAUUIXWDVXABXJTSAROSRUEJVVNPSNNYE";
		    String callBackUrl = body.getCallbackUrl();

		    //set the paymentId
		    paymentResponse.setPaymentId(body.getPaymentId());		
					    
			//Set & unset some values to paymentResponse object in undefined case
			if(isUndefined) {
			    paymentResponse.setCode("000");
			    paymentResponse.setMessage("Aprobado y completado con exito");
			    //set the approved to request of callback
			    paymentResponse.setStatus("approved");
			    paymentResponse.setAuthorizationId("PENDING200");
			    paymentResponse.setPaymentAppData(null);
			    paymentResponse.setDelayToCancel(null);
			    //set some values
                String tId= body.getReference()!=null ? new StringBuilder().append("TID-").append(body.getReference()).toString() : null;
                paymentResponse.setTid(tId);
                String nsu= body.getReference()!=null ? new StringBuilder().append("NSU-").append(body.getReference()).toString() : null;
                paymentResponse.setNsu(nsu);
			    Thread.sleep(3000);
			}

		    LOGGER.info(Utils.toJson(paymentResponse));
		    
			//callback(Utils.toJson(paymentResponse), Utils.toJson(body.getCallbackUrl()));
		    httpResponse = HttpUtil.sendRequest(HttpMethod.POST, Utils.toJson(paymentResponse),callBackUrl,
		                                         MediaType.APPLICATION_JSON,HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON,
		                                         "X-VTEX-API-AppKey",AppKey,"X-VTEX-API-AppToken",AppToken);
		    
		    //save in db
		    if(httpResponse != null) {
		        LOGGER.info(httpResponse.getResponse());
		        //CA:13 TD-813
		        insertTransaction(body.getPaymentId(),merchant,"CALLBACK",body.getTransactionId(),"NULL",body.getReference(),Utils.toJson(paymentResponse),
		        				  Utils.toJson(httpResponse.getResponse()),amount,String.valueOf(httpResponse.getHttpCode()));
		    }

		} catch (IOException | InterruptedException ex) {
		    LOGGER.error(ex.getLocalizedMessage());
		    insertTransaction(body.getPaymentId(), merchant, "NULL", body.getTransactionId(), "NULL", body.getReference(), Utils.toJson(paymentResponse), 
		    				  "ERROR", amount, "NULL");
		}
	}
	
}
