package pe.com.visanet.api.vtex.impl;

import com.quiputech.api.model.Antifraud;
import com.quiputech.api.model.Card;
import com.quiputech.api.model.CardHolder;
import com.quiputech.api.model.Order;
import com.quiputech.ecore.v3.commons.utils.Utils;
import com.quiputech.http.util.HttpResponse;
import com.quiputech.http.util.HttpUtil;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.com.visanet.api.vtex.VtexApiService;
import pe.com.visanet.api.vtex.controller.VtexBase;
import pe.com.visanet.api.vtex.model.CreatePaymentRequest;
import pe.com.visanet.model.AuthorizationRequest;
import pe.com.visanet.model.ConfirmationRequest;
import pe.com.visanet.model.RefundRequest;

/**
 *
 * @author mgaldamez
 */
public class VtexApiServiceImpl extends VtexBase implements VtexApiService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(VtexApiServiceImpl.class);
    
    @Override
    public HttpResponse authorize(CreatePaymentRequest body, String merchant, String appToken) {
        HttpResponse httpResponse = null;
        AuthorizationRequest request = new AuthorizationRequest();
        try {
            String accessToken = securityToken(appToken);

            if (accessToken == null) { 
                LOGGER.error("Unable to generate token with credentials provided: {}", appToken);
                return httpResponse;
            }
            //
            request.setTerminalId(TERMINAL);
            request.setChannel(CHANNEL);
            request.setCaptureType(CAPTURE_TYPE);
            //Order
            Order order = new Order();
            order.setPurchaseNumber(body.getReference());
            order.setAmount(body.getValue());
            order.setCurrency(body.getCurrency());
            order.setExternalTransactionId(body.getPaymentId());
            if(CONTROL){
                if(body.getValue() <= 20 && body.getCurrency().equals("PEN") && body.getCard().getNumber() != null && body.getCard().getNumber().length() >= 6) {
                    String bin = body.getCard().getNumber().substring(0, 6);
                    if(BIN_LIST.contains(bin) && body.getInstallments() == 1) {
                        body.setInstallments(0);
                    }
                }
            }
            order.setInstallment(body.getInstallments() == 1 ? 0 : body.getInstallments());
            request.setOrder(order);
            //Card
            Card card = new Card();
            card.setCardNumber(body.getCard().getNumber());
            int month = body.getCard().getExpiration().getMonth() != null ? Integer.valueOf(body.getCard().getExpiration().getMonth()) : 0;
            card.setExpirationMonth(month);
            String year = body.getCard().getExpiration().getYear() != null ? body.getCard().getExpiration().getYear() : "";
            if (year.length() > 2) {
                year = year.substring(year.length() - 2);
            }
            card.setExpirationYear(!year.isEmpty() ? Integer.valueOf(year) : 0);
            card.setCvv2(body.getCard().getCsc());
            request.setCard(card);
            //Card Holder
            CardHolder cardHolder = new CardHolder();
            Map<String,String> names = getNames(body.getCard().getHolder());
            cardHolder.setFirstName(names.get("firstName"));
            cardHolder.setLastName(names.get("lastName"));
            cardHolder.setEmail(body.getMiniCart().getBuyer().getEmail());
            request.setCardHolder(cardHolder);
            //Antifraud
            Antifraud antifraud = new Antifraud();
            antifraud.setDeviceFingerprintId(body.getDeviceFingerprint());
            antifraud.setClientIp(body.getIpAddress());
            Map<String, String> merchantDefineData = new HashMap<>();
            merchantDefineData.put("MDD4", body.getMiniCart().getBuyer().getEmail());
            merchantDefineData.put("MDD5", body.getTransactionId());
            if (body.getShopperInteraction() != null && body.getShopperInteraction().equals(SHOPPER_INTERACTION)) {
                merchantDefineData.put("MDD14", MDD14);
                card.setRegisterFrequent(false);
                card.setUseFrequent(true);
            }
            merchantDefineData.put("MDD31", body.getMiniCart().getBuyer().getPhone());
            merchantDefineData.put("MDD32", body.getMiniCart().getBuyer().getId());
            merchantDefineData.put("MDD33", body.getMiniCart().getBuyer().getDocumentType());
            merchantDefineData.put("MDD34", body.getMiniCart().getBuyer().getDocument());
            if (body.getMiniCart().getItems() != null) {
                merchantDefineData.put("MDD40", String.valueOf(body.getMiniCart().getItems().length));
            }
            if (body.getMiniCart().getShippingAddress() != null) {
                String shippingAddress = String.format("%s %s %s %s %s", 
                    body.getMiniCart().getShippingAddress().getStreet(), 
                    body.getMiniCart().getShippingAddress().getNumber(), 
                    body.getMiniCart().getShippingAddress().getComplement(), 
                    body.getMiniCart().getShippingAddress().getCity(), 
                    body.getMiniCart().getShippingAddress().getState());            
                merchantDefineData.put("MDD82", shippingAddress);
            }
            antifraud.setMerchantDefineData(merchantDefineData);
            request.setAntifraud(antifraud);

            LOGGER.info("Request Authorization: {}", Utils.toJson(request));
            String endpoint = AUTH_ENDPOINT.replace("{merchantId}", merchant);
            LOGGER.info("Endpoint: {}", endpoint);
            httpResponse = HttpUtil.sendRequest(HttpMethod.POST, Utils.toJson(request), endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);
            LOGGER.info("Response Authorization: {} - {}", httpResponse.getHttpCode(), httpResponse.getResponse());
        } catch(IOException | NumberFormatException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return httpResponse;
    }

    @Override
    public HttpResponse confirm(String appToken, String merchant, String purchaseNumber, String authorizedAmount, String amount, String currency, String trxId) {
        HttpResponse httpResponse = null;
        ConfirmationRequest request = new ConfirmationRequest();
        try {
            String accessToken = securityToken(appToken);
            if (accessToken == null) { 
                LOGGER.error("Unable to generate token with credentials provided: {}", appToken);
                return httpResponse;
            }
            LOGGER.info("Token: {}", accessToken);
            //
            request.setTerminalId(TERMINAL);
            request.setChannel(CHANNEL);
            request.setCaptureType(CAPTURE_TYPE);
            //Order
            Order order = new Order();
            order.setPurchaseNumber(purchaseNumber);
            order.setAmount(Double.valueOf(amount));
            order.setAuthorizedAmount(Double.valueOf(authorizedAmount));
            order.setCurrency(currency);
            order.setTransactionId(trxId);
            request.setOrder(order);
            
            LOGGER.info("Request confirm: {}", Utils.toJson(request));
            String endpoint = CONFIRM_ENDPOINT.replace("{merchantId}", merchant);
            LOGGER.info("Endpoint: {}", endpoint);
            httpResponse = HttpUtil.sendRequest(HttpMethod.POST, Utils.toJson(request), endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);
            LOGGER.info("Response confirm: {}", httpResponse.toString());
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return httpResponse;
    }
    
    @Override
    public HttpResponse refund(String appToken, String merchant, String transactionId, String ruc, String amount) {
        HttpResponse httpResponse = null;
        RefundRequest request = new RefundRequest();
        try {
            String accessToken = securityToken(appToken);
            if (accessToken == null) { 
                LOGGER.error("Unable to generate token with credentials provided: {}", appToken);
                return httpResponse;
            }
            LOGGER.info("Token: {}", accessToken);
            //
            request.setRuc(ruc);
            request.setComment("");
            request.setExternalReferenceId(transactionId);
            request.setAmount(Double.valueOf(amount));

            LOGGER.info("Request refund: {}", Utils.toJson(request));
            String endpoint = REFUND_ENDPOINT.replace("{merchantId}", merchant).replace("{transactionId}", transactionId);
            LOGGER.info("Endpoint: {}", endpoint);
            httpResponse = HttpUtil.sendRequest(HttpMethod.POST, Utils.toJson(request), endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);
            LOGGER.info("Response refund: {}", httpResponse.toString());
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return httpResponse;
    }
    
    @Override
    public HttpResponse reverse(String appToken, String merchant, String purchaseNumber, String trxDate) {
        HttpResponse httpResponse = null;
        AuthorizationRequest request = new AuthorizationRequest();
        try {
            String accessToken = securityToken(appToken);
            if (accessToken == null) { 
                LOGGER.error("Unable to generate token with credentials provided: {}", appToken);
                return httpResponse;
            }
            
            LOGGER.info("Token: " + accessToken);
            //Order
            Order order = new Order();
            order.setPurchaseNumber(purchaseNumber);
            order.setTransactionDate(trxDate);
            request.setOrder(order);

            LOGGER.info("Request reverse: {}", Utils.toJson(request));
            String endpoint = VOID_ENDPOINT.replace("{merchantId}", merchant);
            LOGGER.info("Endpoint: {}",endpoint);
            httpResponse = HttpUtil.sendRequest(HttpMethod.POST, Utils.toJson(request), endpoint, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, accessToken);
            LOGGER.info("Response reverse: {}", httpResponse.toString());
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return httpResponse;
    }

    @Override
    public String securityToken(String securityCredentials) {
        HttpResponse httpResponse = null;
        try {
            httpResponse = HttpUtil.sendRequest(HttpMethod.GET, "", SECURITY_ENDPOINT, MediaType.APPLICATION_JSON, HttpHeaders.AUTHORIZATION, securityCredentials);
        } catch(IOException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return httpResponse != null && httpResponse.getHttpCode() == 201 ? httpResponse.getResponse() : null;
    }
        
    public Map<String, String> getNames(String fullName) {
        int start = fullName.indexOf(' ');
        int end = fullName.lastIndexOf(' ');

        Map<String, String> names = new HashMap<>();
        String firstName = "";
        String middleName = "";
        String lastName = "";

        if (start >= 0) {
            firstName = fullName.substring(0, start);
            if (end > start) {
                middleName = fullName.substring(start + 1, end);
            }
            lastName = middleName + " " + fullName.substring(end + 1, fullName.length());
        } else {
            firstName = fullName;
            lastName = "D";
        }
        names.put("firstName", firstName);
        names.put("middleName", middleName);
        names.put("lastName", lastName);
        return names;
    }
}
