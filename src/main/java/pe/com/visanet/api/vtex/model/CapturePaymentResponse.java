/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "CapturePaymentRequest", description = "Captures (settle) a payment that was previously approved.")
public class CapturePaymentResponse {
    private String paymentId;
    private String settleId;
    private double value;
    private String code;
    private String message;
    private String requestId;

    @ApiModelProperty(value = "VTEX PaymentId enviado en el request", example = "191209022")
    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    @ApiModelProperty(value = "Provider's settlement identifier ", example = "3a43a2c8-0659-44e6-b098-5cbc5641ae3f")
    public String getSettleId() {
        return settleId;
    }

    public void setSettleId(String settleId) {
        this.settleId = settleId;
    }

    @ApiModelProperty(value = "The amount to be settled/captured", example = "12.99")
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @ApiModelProperty(value = "Provider's operation/error code to be logged", example = "000")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ApiModelProperty(value = "Provider's operation/error message to be logged", example = "Aprobado y completado con exito")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @ApiModelProperty(value = "The same requestId sent in the request", example = "D12D9B80972C462980F5067A3A126846")
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
    
}
