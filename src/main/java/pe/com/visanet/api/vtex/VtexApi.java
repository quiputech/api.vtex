package pe.com.visanet.api.vtex;

import com.quiputech.api.swagger.annotations.SwaggerAnnotations;
import com.quiputech.ecore.v3.commons.utils.Utils;
import com.quiputech.security.annotations.Logged;
import com.quiputech.security.annotations.NoSecurity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.visanet.api.vtex.model.CancelPaymentRequest;
import pe.com.visanet.api.vtex.model.CancelPaymentResponse;
import pe.com.visanet.api.vtex.model.CapturePaymentRequest;
import pe.com.visanet.api.vtex.model.CapturePaymentResponse;
import pe.com.visanet.api.vtex.model.CreateAuthorizationRequest;
import pe.com.visanet.api.vtex.model.CreateAuthorizationResponse;
import pe.com.visanet.api.vtex.model.CreatePaymentRequest;
import pe.com.visanet.api.vtex.model.CreatePaymentResponse;
import pe.com.visanet.api.vtex.model.GenericError;
import pe.com.visanet.api.vtex.model.PaymentMethods;
import pe.com.visanet.api.vtex.model.RefundPaymentRequest;
import pe.com.visanet.api.vtex.model.RefundPaymentResponse;
import pe.com.visanet.api.vtex.controller.VtexController;

/**
 *
 * @author Michael Galdámez
 */
@Path("")
@PermitAll
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "VTEX - Payment Provider Protocol", description = "RESTful API for payments")
public class VtexApi {

    @GET()
    @Path("/payment-methods")
    @Produces("application/json")
    @Logged
    @NoSecurity
    public Response getPaymentMethods(@Context HttpServletRequest servletRequest) {
        PaymentMethods paymentMethods = new PaymentMethods();
        
        String[] methods = {"Visa", "Mastercard", "American Express", "Diners", "BankInvoice", "Privatelabels", "Promissories"};
        paymentMethods.setPaymentMethods(methods);
        
        return Response.ok(paymentMethods).build();
    }
    
    @GET()
    @Path("/manifest")
    @Produces("application/json")
    @Logged
    @NoSecurity
    public Response getListPaymentManifest(@Context HttpServletRequest servletRequest) {
        String response = "{\"paymentMethods\":[{\"name\":\"Visa\",\"allowsSplit\":\"disabled\"},{\"name\":\"Mastercard\",\"allowsSplit\":\"disabled\"},{\"name\":\"American Express\",\"allowsSplit\":\"disabled\"},"
                + "{\"name\":\"Diners\",\"allowsSplit\":\"disabled\"},{\"name\":\"BankInvoice\",\"allowsSplit\":\"disabled\"},{\"name\":\"Privatelabels\",\"allowsSplit\":\"disabled\"},"
                + "{\"name\":\"Promissories\",\"allowsSplit\":\"disabled\"},{\"name\":\"Niubiz\",\"allowsSplit\":\"disabled\"}],\"autoSettleDelay\":{\"minimum\":\"0\",\"maximum\":\"720\"}}";
        return Response.ok(response).build();
    }
    
    @POST
    @Path("/authorization/token")
    @ApiOperation(value = "Create Authorization Token", notes = "RESTful API to Create Authorization Token.", response = CreateAuthorizationResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Managed error."),
                           @ApiResponse(code = 401, message = SwaggerAnnotations.UNAUTHORIZED),
                           @ApiResponse(code = 500, message = SwaggerAnnotations.REQUEST_NOT_VALID)})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Logged
    @NoSecurity
    public Response createToken(CreateAuthorizationRequest request) {
        String applicationId = request.getApplicationId();
        CreateAuthorizationResponse response = new CreateAuthorizationResponse();
        response.setApplicationId(applicationId);
        response.setToken(UUID.randomUUID().toString());
        return Response.ok(response).build();
    }
    
    @POST
    @Path("/payments")
    @ApiOperation(value = "Create Payment", notes = "RESTful API to Autorize Payments", response = CreatePaymentResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 401, message = SwaggerAnnotations.UNAUTHORIZED),
                           @ApiResponse(code = 500, message = SwaggerAnnotations.REQUEST_NOT_VALID)})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Logged
    @NoSecurity
    public Response autorize(@ApiParam(value = "Create Payment Request", required = true) CreatePaymentRequest body, @Context HttpHeaders headers) {
        GenericError error = new GenericError("400", "Request body is wrong.");
        VtexController controller = new VtexController();
        String merchant = headers.getRequestHeader("X-VTEX-API-AppKey").get(0);
        String appToken = headers.getRequestHeader("X-VTEX-API-AppToken").get(0);
        String isTestSuite = headers.getRequestHeader("X-VTEX-API-Is-TestSuite").isEmpty() ? null : headers.getRequestHeader("X-VTEX-API-Is-TestSuite").get(0);
        boolean validMerchantId = !StringUtils.isEmpty(merchant) && StringUtils.isNumeric(merchant) && StringUtils.length(merchant) <= 9;
        if (!validMerchantId){
            String errorCode = "400";
            String errorMsg = "X-VTEX-API-AppKey is not valid";
            error = new GenericError(errorCode, errorMsg);
            return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
        }
        CreatePaymentResponse paymentResponse = controller.createPayment(merchant, appToken, body, isTestSuite);
        if(paymentResponse != null && paymentResponse.getStatus().equals("CustomError")) {
            String errorCode = paymentResponse.getCode();
            String errorMsg = paymentResponse.getMessage();
            error = new GenericError(errorCode, errorMsg);
            error.setStatus("error");
            controller.insertTransaction(body.getPaymentId(), merchant, "AUTH", body.getTransactionId(), 
            "NULL", "", Utils.toJson(body), Utils.toJson(error), String.valueOf(body.getValue()), error.getCode());
            return Response.status(Integer.parseInt(errorCode)).entity(error).build();
        }
        if (paymentResponse == null) {
            error.setStatus("error");
            error.setCode("500");
            error.setMessage("Authorization has failed due to an internal error");
            controller.insertTransaction(body.getPaymentId(), merchant, "AUTH", body.getTransactionId(), 
            "NULL", body.getReference(), Utils.toJson(body), Utils.toJson(error), String.valueOf(body.getValue()), error.getCode());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).build();
        } else {
            return Response.ok(paymentResponse).build();
        }
    }
    
    @POST
    @Path("/payments/{paymentId}/cancellations")
    @ApiOperation(value = "Cancel Payment", notes = "RESTful API to Cancel a payment", response = CancelPaymentResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 401, message = SwaggerAnnotations.UNAUTHORIZED),
                           @ApiResponse(code = 500, message = SwaggerAnnotations.REQUEST_NOT_VALID)})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Logged
    @NoSecurity
    public Response reverse( 
        @ApiParam(value = "Identificador del pago", required = true, example = "191209014") @PathParam("paymentId") String paymentId,
        @ApiParam(value = "cancel request", required = true) CancelPaymentRequest body, @Context HttpHeaders headers) {
        GenericError error = new GenericError("400", "Request body is wrong.");
        VtexController controller = new VtexController();
        String merchant = headers.getRequestHeader("X-VTEX-API-AppKey").get(0);
        String appToken = headers.getRequestHeader("X-VTEX-API-AppToken").get(0);
        CancelPaymentResponse paymentResponse = controller.cancelPayment(paymentId, merchant, appToken, body);
        if (paymentResponse == null) {
            error.setStatus("error");
            error.setCode("500");
            error.setMessage("Authorization has failed due to an internal error");
            controller.insertTransaction(body.getPaymentId(), merchant, "REVERSE", "", 
            body.getRequestId(), "", Utils.toJson(body), Utils.toJson(error), "", error.getCode());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).build();
        }
        return paymentResponse.getCode().equals("400") 
                ? Response.ok(paymentResponse).build()
                : Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(paymentResponse).build();
    }
    
    @POST
    @Path("/payments/{paymentId}/settlements")
    @ApiOperation(value = "Capture Payment", notes = "RESTful API to Captures (settle) a payment that was previously approved.", response = CapturePaymentResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 401, message = SwaggerAnnotations.UNAUTHORIZED),
                           @ApiResponse(code = 500, message = SwaggerAnnotations.REQUEST_NOT_VALID)})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Logged
    @NoSecurity
    public Response capture( 
        @ApiParam(value = "Identificador del pago", required = true, example = "191209014") @PathParam("paymentId") String paymentId, 
        @ApiParam(value = "capture request", required = true) CapturePaymentRequest body, @Context HttpHeaders headers) {
        GenericError error = new GenericError("400", "Request body is wrong.");
        VtexController controller = new VtexController();
        String merchant = headers.getRequestHeader("X-VTEX-API-AppKey").get(0);
        String appToken = headers.getRequestHeader("X-VTEX-API-AppToken").get(0);
        CapturePaymentResponse paymentResponse = controller.capturePayment(paymentId, merchant, appToken, body);
        if (paymentResponse == null) {
            error.setStatus("error");
            error.setCode("500");
            error.setMessage("Authorization has failed due to an internal error");
            controller.insertTransaction(body.getPaymentId(), merchant, "CONFIRM", body.getTransactionId(), 
            "NULL", "", Utils.toJson(body), Utils.toJson(error), String.valueOf(body.getValue()), error.getCode());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).build();
        }
        return paymentResponse.getCode().equals("000") || paymentResponse.getCode().equals("010")
            ? Response.ok(paymentResponse).build()
            : Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(paymentResponse).build();
    }    
    
    @POST
    @Path("/payments/{paymentId}/refunds")
    @ApiOperation(value = "Refund Payment", notes = "RESTful API to Refunds a payment that was previously captured (settled).", response = RefundPaymentResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 401, message = SwaggerAnnotations.UNAUTHORIZED),
                           @ApiResponse(code = 500, message = SwaggerAnnotations.REQUEST_NOT_VALID)})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Logged
    @NoSecurity
    public Response refund( 
        @ApiParam(value = "Identificador del pago", required = true, example = "191209014") @PathParam("paymentId") String paymentId, 
        @ApiParam(value = "refund request", required = true) RefundPaymentRequest body, @Context HttpHeaders headers) {
        GenericError error = new GenericError("400", "Request body is wrong.");
        VtexController controller = new VtexController();
        String merchant = headers.getRequestHeader("X-VTEX-API-AppKey").get(0);
        String appToken = headers.getRequestHeader("X-VTEX-API-AppToken").get(0);
        RefundPaymentResponse paymentResponse = controller.refundPayment(paymentId, merchant, appToken, body);
        if (paymentResponse == null) {
            error.setStatus("error");
            error.setCode("500");
            error.setMessage("Authorization has failed due to an internal error");
            controller.insertTransaction(body.getPaymentId(), merchant, "REFUND", body.getTransactionId(), 
            "NULL", "", Utils.toJson(body), Utils.toJson(error), String.valueOf(body.getValue()), error.getCode());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).build();
        }
        return paymentResponse.getCode().equals("100") || paymentResponse.getCode().equals("110")
            ? Response.ok(paymentResponse).build()
            : Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(paymentResponse).build();
    }   
}
