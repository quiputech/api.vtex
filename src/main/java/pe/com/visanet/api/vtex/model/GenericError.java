/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
/**
 *
 * @author Michael Galdámez
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "GenericError", description = "Error Generico")
public class GenericError implements java.io.Serializable, Cloneable {
    //private Integer errorCode;
    private String status;
    private String code;
    private String message;
    //private Map<String, String> data;

    public GenericError() {
        
    }
    
    public GenericError(String errorCode, String errorMessage) {
        this.code = errorCode;
        this.message = errorMessage;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    protected final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

