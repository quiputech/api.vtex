package pe.com.visanet.api.vtex.driver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.com.visanet.api.vtex.controller.VtexBase;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class BaseDriver extends VtexBase {

    protected static final String DATASOURCE = "java:/ecorev3DS";
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseDriver.class);
    private DataSource ds = null;

    public Connection getConnection(){
        try {
            javax.naming.Context ctx = new InitialContext();
            ds = (DataSource)ctx.lookup(DATASOURCE);
            return ds.getConnection();
        } catch (SQLException | NamingException e) {
            LOGGER.error(e.getLocalizedMessage());
            return null;
        }
    }

   protected void close(Object ... objects) {
        try {
            for(Object object : objects) {
                if(object instanceof ResultSet) {
                    ResultSet rs = (ResultSet) object;
                    if(!rs.isClosed()) {
                        rs.close();
                    }
                }
                if(object instanceof PreparedStatement) {
                    PreparedStatement pstmt = (PreparedStatement) object;
                    if(!pstmt.isClosed()) {
                        pstmt.close();
                    }
                }
                if(object instanceof Connection) {
                    Connection conn = (Connection) object;
                    if(!conn.isClosed()) {
                        conn.close();
                    }
                }
            }
        } catch(SQLException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }
    }

}