package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "paymentMethods", description = "Return all payment methods that your provider can handle.")
public class PaymentMethods {
    private String[] paymentMethods;

    @ApiModelProperty(value = "Arreglo de metodos de pago", required = true, example = ""
                + "{\n" +
                "  \"paymentMethods\": [\n" +
                "    \"Visa\",\n" +
                "    \"Mastercard\",\n" +
                "    \"American Express\",\n" +
                "    \"BankInvoice\",\n" +
                "    \"Privatelabels\",\n" +
                "    \"Promissories\"\n" +
                "  ]\n" +
                "}")
    public String[] getPaymentMethods() {
        return paymentMethods;
    }

    @JsonProperty("paymentMethods")
    public void setPaymentMethods(String[] paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
}
