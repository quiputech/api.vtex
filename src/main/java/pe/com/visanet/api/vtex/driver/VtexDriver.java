package pe.com.visanet.api.vtex.driver;

import com.quiputech.niubiz.utils.NiubizLogger;
import com.quiputech.niubiz.utils.entity.LogData;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mgaldamez
 */
public class VtexDriver extends BaseDriver {
    private static final Logger LOGGER = LoggerFactory.getLogger(VtexDriver.class);
    public static final String GET_SELECT_TRANSACTION = "SELECT id, channel, acquirer, capture_type, commerce_id, transaction_id, id_resolutor, "
            + "id_procesador, trx_date, po_number, po_ext_number, currency, authorization_code, order_amount, authorization_date, signature "
            + "FROM backofficevn.bo_sales WHERE po_ext_number = ? AND channel = ? AND state = ? AND commerce_id = ?";
    
    public static final String SELECT_TRANSACTION = "SELECT signature, authorization_code, id_procesador, entity_id, state, action_code, authorization_message "
            + "FROM backofficevn.bo_sales WHERE po_number = ? AND commerce_id = ?";
    
    public static final String INSERT_TRANSACTION = " INSERT INTO `backofficevn`.`vtex` (`paymentId`, `merchantId`, `operation`, `transactionId`, `requestId`, " 
            + "`purchaseNumber`, `transactionDate`, `request`, `response`, `amount`, `status`) VALUES ( ?, ?, ?, ?, ?, ?, sysdate(), ?, ?, ?, ?);";
    
    public static final String SELECT_VTEX_PAYMENT = "SELECT Count(*) AS RESULT FROM backofficevn.vtex WHERE paymentId = ? AND merchantId = ? AND transactionId = ?";

    public static final String SELECT_MERCHANT = "SELECT * FROM backofficevn.bo_commerce where commerce_code = ?";
    
    public Map<String,String> getPayment(String paymentId, String merchantId, String state) {

        LOGGER.info("CONSULTA: {}", GET_SELECT_TRANSACTION);
        
        Map<String,String> data = null;

        try (Connection conx = getConnection(); 
            PreparedStatement stmt = conx.prepareStatement(GET_SELECT_TRANSACTION)) {

            LOGGER.info("Values: {} - {} - {} - {}", paymentId, CHANNEL, state, merchantId);
            stmt.setString(1, paymentId);
            stmt.setString(2, CHANNEL);
            stmt.setString(3, state);
            stmt.setString(4, merchantId);

            try (ResultSet rs = stmt.executeQuery();){
                while (rs.next()) {
                    data = new HashMap<>();
                    data.put("purchaseNumber", rs.getString("po_number"));
                    data.put("transactionDate", rs.getString("authorization_date"));
                    data.put("transactionId", rs.getString("transaction_id"));
                    data.put("amount", rs.getString("order_amount"));
                    data.put("currency", rs.getString("currency"));
                    data.put("id_procesador", rs.getString("id_procesador"));
                    data.put("id_resolutor", rs.getString("id_resolutor"));
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
        return data;
    }
    
    public Map<String,String> getPaymentUndefined(String purchaseNumber, String merchantId) {
    
        LOGGER.info("CONSULTA: {}", SELECT_TRANSACTION);
        
        Map<String,String> data = null;

        try (Connection conx = getConnection(); 
            PreparedStatement stmt = conx.prepareStatement(SELECT_TRANSACTION)) {

            LOGGER.info("Values: {} - {}", purchaseNumber, merchantId);
            stmt.setString(1, purchaseNumber);
            stmt.setString(2, merchantId);

            try (ResultSet rs = stmt.executeQuery();){
                while (rs.next()) {
                    data = new HashMap<>();
                    data.put("signature", rs.getString("signature"));
                    data.put("authorization_code", rs.getString("authorization_code"));
                    data.put("id_procesador", rs.getString("id_procesador"));
                    data.put("entity_id", rs.getString("entity_id"));
                    data.put("state", rs.getString("state"));
                    data.put("action_code", rs.getString("action_code"));
                    data.put("authorization_message", rs.getString("authorization_message"));
                }
            }
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
        return data;
    }
    
    public long getTransaction(Connection cnx, String transactionId, String paymentId, String merchantId){
        LOGGER.info("CONSULTA: {}", SELECT_VTEX_PAYMENT);

        long result = 0;
        ResultSet rs = null;
        Connection conx = null;
        PreparedStatement stmt = null;

        try {
            LOGGER.info("Values: {} - {} - {}", paymentId, merchantId, transactionId);
            conx = getConnection();
            stmt = conx.prepareStatement(SELECT_VTEX_PAYMENT);
            stmt.setString(1, paymentId);
            stmt.setString(2, merchantId);
            stmt.setString(3, transactionId);

            rs = stmt.executeQuery();

            while (rs.next()) {
                result = rs.getLong("RESULT");
            }
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage());
        } finally {
            close(rs, stmt, conx);
        }
        return result;
    }
    
    public Map<String, String> getMerchantData(String merchant) {
        Connection conx = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<String, String> params = null;
        LOGGER.info("GET_MERCHANT_DATA: {}", SELECT_MERCHANT.replace("?", merchant));
        try {
            conx = getConnection();
            stmt = conx.prepareStatement(SELECT_MERCHANT);
            stmt.setString(1, merchant);
            rs = stmt.executeQuery();
            
            if (rs.next()) {
                params = new HashMap<>();
                params.put("ruc", rs.getString("ruc"));
                params.put("maxAmount", rs.getString("authorized_max_percentage"));
                params.put("automatedSettle", rs.getString("automated_settle"));
            }
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } finally {
            close(rs, stmt, conx);
        }
        return params;
    }
    
    public long insertTransaction(String paymentId, String merchantId, String operation, String transactionId, String requestId, String purchaseNumber,
        String request, String response, String amount, String status) {
        LOGGER.info("INSERT: {}", INSERT_TRANSACTION);

        long result = 0;
        Connection conx = null;
        PreparedStatement stmt = null;
        String eventType = "ERROR";
        String mskRequest = operation.equals("AUTH") ? maskingData(new JSONObject(request)).toString() : request;

        try {
            if(LOG_ENABLED){
                conx = getConnection();
                stmt = conx.prepareStatement(INSERT_TRANSACTION);
                stmt.setObject(1, paymentId);
                stmt.setObject(2, merchantId);
                stmt.setObject(3, operation);
                stmt.setObject(4, transactionId);
                stmt.setObject(5, requestId);
                stmt.setObject(6, purchaseNumber);
                stmt.setObject(7, mskRequest);
                stmt.setObject(8, response);
                stmt.setObject(9, amount);
                stmt.setObject(10, status);
                result = stmt.executeUpdate();
            }
            eventType = "INFO";
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } finally {
            close(stmt, conx);
        }
        printLog(paymentId, merchantId, operation, transactionId, requestId, purchaseNumber, mskRequest, response, amount, status, operation, eventType);
        return result;
    }
    
    private void printLog(String paymentId, String merchantId, String operation, String transactionId, String requestId, String purchaseNumber,
            String request, String response, String amount, String status, String processType, String eventType){
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        JsonObjectBuilder builder = Json.createObjectBuilder();
        //paymentId,merchantId,operation,transactionId,requestId,purchaseNumber,transactionDate,request,response,amount,status
        builder.add("paymentId", setDeafultValue(paymentId));
        builder.add("merchantId", setDeafultValue(merchantId));
        builder.add("operation", setDeafultValue(operation));
        builder.add("transactionId", setDeafultValue(transactionId));
        builder.add("requestId", setDeafultValue(requestId));
        builder.add("purchaseNumber", setDeafultValue(purchaseNumber));
        builder.add("transactionDate", setDeafultValue(now));
        builder.add("request", setDeafultValue(request));
        builder.add("response", setDeafultValue(response));
        builder.add("amount", setDeafultValue(amount));
        builder.add("status", setDeafultValue(status));
        
        LogData logData = new LogData(purchaseNumber,merchantId,"api.vtex",processType,"","","","",null,eventType, now, builder.build());
        NiubizLogger niubizLog = new NiubizLogger();
        niubizLog.printLog(logData);
    }
    
    public String setDeafultValue(String value){
        String defaultStr = value!= null ? value : "";
        return defaultStr;
    }
    
    public JSONObject maskingData(JSONObject jsonRequest){
        String number = "";
        String csc = "";
        JSONObject jsonCard = jsonRequest.getJSONObject("card");
        if(jsonCard != null){
            number = jsonCard.optString("number","");
            csc = jsonCard.optString("csc","");
            if(!number.equals("")){
                jsonCard.put("number", mask(number));
                jsonRequest.put("card", jsonCard);
            }
            if(!csc.equals("")){
                jsonCard.put("csc", mask(csc));
                jsonRequest.put("card", jsonCard);
            }
        }
        return jsonRequest;
    }
    
    public static String mask(String data) {

        if (data.equals("")) {
            return data;
        }

        char[] toCharArray = data.toCharArray();
        if (data.length() < 16){
            for (int i = 0; i < data.length(); i++) {
                toCharArray[i] = '*';
            }
        } else {
            for (int i = 6; i < data.length() - 4; i++) {
                toCharArray[i] = '*';
            }
        }
        
        return new String(toCharArray);
    }
}
