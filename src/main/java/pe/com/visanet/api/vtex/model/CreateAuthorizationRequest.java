/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.api.vtex.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "CreateAuthorizationRequest", description = "Create Authorization Request")
public class CreateAuthorizationRequest {
    private String applicationId;
    private String returnUrl;

    @ApiModelProperty(value = "VTEX application identifier (always vtex)", example = "VTEX")
    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @ApiModelProperty(value = "URL", example = "https://admin.mystore.example.com/provider-return?authorizationCode=")
    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }
    
}
