/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.quiputech.api.model.Header;
import com.quiputech.api.model.Headline;
import com.quiputech.api.model.Order;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;

/**
 *
 * @author Peter Galdamez
 */
@JsonInclude(Include.NON_NULL)
public class AuthorizationResponse {
    private Header header;
    private Headline fulfillment;
    private Order order;
    private Map<String, String> dataMap;
    private Map<String, String> data;
    private int errorCode;
    private String errorMessage;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @ApiModelProperty(value = "Entidad que representa el resultado de la transaccion", required = true)
    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @ApiModelProperty(value = "Entidad que representa el encabezado de la transaccion")
    public Headline getFulfillment() {
        return fulfillment;
    }

    public void setFulfillment(Headline fulfillment) {
        this.fulfillment = fulfillment;
    }

    @ApiModelProperty(value = "Entidad que representa el pedido", required = true)
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
    
    @ApiModelProperty(value = "Informacion complementaria de la operacion")
    public Map<String, String> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, String> dataMap) {
        this.dataMap = dataMap;
    }
    
    @ApiModelProperty(value = "Informacion complementaria de la operacion")
    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }        
}
