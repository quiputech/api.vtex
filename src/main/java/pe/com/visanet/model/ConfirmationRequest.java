/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quiputech.api.model.Order;
import com.quiputech.api.model.Sponsored;
import com.quiputech.ecore.v3.commons.core.DataMap;

/**
 *
 * @author Peter Galdamez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmationRequest {
    private String terminalId;
    private String channel;
    private String captureType;
    private String lon;
    private String lat;
    private Order order;
    private Sponsored sponsored;
    private DataMap<String, Object> dataMap;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCaptureType() {
        return captureType;
    }

    public void setCaptureType(String captureType) {
        this.captureType = captureType;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Sponsored getSponsored() {
        return sponsored;
    }

    public void setSponsored(Sponsored sponsored) {
        this.sponsored = sponsored;
    }

    public DataMap<String, Object> getDataMap() {
        return dataMap;
    }

    public void setDataMap(DataMap<String, Object> dataMap) {
        this.dataMap = dataMap;
    }
}
