/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quiputech.api.model.Antifraud;
import com.quiputech.api.model.Authentication;
import com.quiputech.api.model.Card;
import com.quiputech.api.model.CardHolder;
import com.quiputech.api.model.Order;
import com.quiputech.api.model.Recurrence;
import com.quiputech.api.model.Sponsored;
import com.quiputech.api.model.dcc.CurrencyConversion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Map;

/**
 *
 * @author Peter Galdamez
 */
@ApiModel(value="AuthorizationRequest", description="Authorize transaction request")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationRequest {
    private String terminalId;
    private String channel;
    private Boolean pii;
    private boolean countable;
    private boolean terminalUnattended;
    private boolean fastPayment;
    private boolean qr;
    private Integer mttIdentifier;
    private String captureType;
    private String lon;
    private String lat;
    private Order order;
    private Card card;
    private CardHolder cardHolder;
    private Sponsored sponsored;
    private Antifraud antifraud;
    private Authentication authentication;
    private CurrencyConversion currencyConversion;
    private Recurrence recurrence;
    private Map<String, String> dataMap;

    @ApiModelProperty(value = "Numero de Terminal", required = true, example = "12345678")
    public String getTerminalId() {
        if (terminalId == null)
            terminalId = "";
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    @ApiModelProperty(value = "Canal desde donde se origino la transaccion", allowableValues = "mobile,web,mpos,host,pos,kiosk,callcenter,recurrent", required = true, example = "web")
    public String getChannel() {
        if (channel == null)
            channel = "";
        
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @ApiModelProperty(value = "Flag para indicar si la transaccion devolvera informacion personal en caso de ser permitido")
    public Boolean getPii() {
        return pii;
    }

    public void setPii(Boolean pii) {
        this.pii = pii;
    }
    
    @ApiModelProperty(value = "Flag para indicar si la transaccion vino desde canal desatendido")
    public boolean isTerminalUnattended() {
        return terminalUnattended;
    }

    public void setTerminalUnattended(boolean terminalUnattended) {
        this.terminalUnattended = terminalUnattended;
    }

    @ApiModelProperty(value = "Flag para indicar si la transaccion debe procesarse como pago rapido")
    public boolean isFastPayment() {
        return fastPayment;
    }

    public void setFastPayment(boolean fastPayment) {
        this.fastPayment = fastPayment;
    }

    @ApiModelProperty(value = "Flag para indicar si la transaccion ha sido leida de codigo QR")
    public boolean isQr() {
        return qr;
    }

    public void setQr(boolean qr) {
        this.qr = qr;
    }

    @ApiModelProperty(value = "Flag para indicar si la transaccion debe considerar la informacion de MTT")
    public Integer getMttIdentifier() {
        return mttIdentifier;
    }

    public void setMttIdentifier(Integer mttIdentifier) {
        this.mttIdentifier = mttIdentifier;
    }

    @ApiModelProperty(value = "Tipo de captura en el canal", allowableValues = "manual,chip,band,contactless,cardOnFile", required = true, example = "manual")
    public String getCaptureType() {
        if (captureType == null)
            captureType = "";
        return captureType;
    }

    public void setCaptureType(String captureType) {
        this.captureType = captureType;
    }

    @ApiModelProperty(value = "Entidad que representa el pedido", required = true)
    public Order getOrder() {
        if (order == null)
            order = new Order();
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @ApiModelProperty(value = "Entidad que representa al tarjetahabiente", required = true)
    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @ApiModelProperty(value = "Datos del cliente")
    public CardHolder getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(CardHolder cardHolder) {
        this.cardHolder = cardHolder;
    }

    @ApiModelProperty(value = "Entidad que representa al comercio patrocinado. Es obligatorio para Payments Facilitator")
    public Sponsored getSponsored() {
        return sponsored;
    }

    public void setSponsored(Sponsored sponsored) {
        this.sponsored = sponsored;
    }
    	
    @ApiModelProperty(value = "Entidad que maneja la informacion del antifraude")
    public Antifraud getAntifraud() {
        return antifraud;
    }

    public void setAntifraud(Antifraud antifraud) {
        this.antifraud = antifraud;
    }

    @ApiModelProperty(value = "Entidad opcional que maneja la autenticacion para la marca")
    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    @ApiModelProperty(value = "Coordenadas de longitud")
    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @ApiModelProperty(value = "Coordenadas de latitud")
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @ApiModelProperty(value = "Flag que determina si la autorizacion es contable o no")
    public boolean isCountable() {
        return countable;
    }

    public void setCountable(boolean countable) {
        this.countable = countable;
    }

    @ApiModelProperty(value = "Informacion adicional para conversion de moneda")
    public CurrencyConversion getCurrencyConversion() {
        return currencyConversion;
    }

    public void setCurrencyConversion(CurrencyConversion currencyConversion) {
        this.currencyConversion = currencyConversion;
    }
    
    @ApiModelProperty(value = "Informacion adicional para transacciones recurrentes")
    public Recurrence getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }
    
    @ApiModelProperty(value = "Informacion complementaria de la transaccion")
    public Map<String, String> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<String, String> dataMap) {
        this.dataMap = dataMap;
    } 
    
}
