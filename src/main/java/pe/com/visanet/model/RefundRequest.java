/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Michael Galdámez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "RefundRequest", description = "Request de refund")
public class RefundRequest {
    private String ruc;
    private String comment;
    private String externalReferenceId;
    private double amount;
    private String transactionId;

    @ApiModelProperty(value = "ruc del comercio", required = true, example = "20341198217")
    public String getRuc() {
        return ruc;
    }

    @JsonProperty("ruc")
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    @ApiModelProperty(value = "Comentario de la transacción", required = false, example = "")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @ApiModelProperty(value = "Identificador unico de la referencia", required = true, example = "91231202")
    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    @JsonProperty("externalReferenceId")
    public void setExternalReferenceId(String externalReferenceId) {
        this.externalReferenceId = externalReferenceId;
    }

    @ApiModelProperty(value = "Monto de la transacción", required = true, example = "3.00")
    public double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @ApiModelProperty(value = "Identificador unico de la transacción", required = false, example = "991170180003769")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transactionId")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
